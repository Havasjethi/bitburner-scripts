import * as path from "https://deno.land/std@0.210.0/path/mod.ts";
import { SERVER_PORT } from "./src/consts.ts";

const PORT = SERVER_PORT;
const FOLDER_TO_READ = "dist";

// TODO :: Cache only if FS watching added
// const cache: Record<string, string> = {};

export async function get_file(filename: string): Promise<string> {
  const result = path.join(FOLDER_TO_READ, filename);
  console.log(result);

  return Deno.readTextFile(result);
}

export async function handler(request: Request): Promise<Response> {
  const url = new URL(request.url);
  console.log("Incoming request: " + url);

  if (!url.pathname.endsWith(".js")) {
    return new Response("Only js 'resources' are supported", { status: 404 });
  }

  // const file_content = (cache[url.pathname] ||= await get_file(url.pathname));
  const file_content = await get_file(url.pathname);
  return new Response(file_content, {
    status: 200,
    headers: {
      "access-control-allow-origin": "*",
    },
  });
}

console.log(`HTTP server running. Access it at: http://localhost:${PORT}/`);
Deno.serve({
  hostname: "127.0.0.1",
  port: PORT,
}, handler);
