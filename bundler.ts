import { bundle } from "https://deno.land/x/emit/mod.ts";

const watcher = Deno.watchFs("src");
const encoder = new TextEncoder();

const latching_tool = new (class SomeEvail {
  private _some: Record<string, number> = {};

  up(paths: string[]) {
    for (const p of paths) {
      this._some[p] = (this._some[p] || 0) + 1;
    }
  }

  down(paths: string[]): string[] {
    const filtered = [];
    for (const p of paths) {
      const new_value = (this._some[p] || 1) - 1;
      this._some[p] = new_value;
      if (new_value === 0) {
        filtered.push(p);
      }
    }

    return filtered;
  }
})();

for await (const event of watcher) {
  // console.log(event);
  if (event.kind !== "modify") {
    continue;
  }

  const paths = event.paths.filter((e) => e.includes("/bin/"));
  if (paths.length === 0) {
    continue;
  }

  latching_tool.up(paths);

  setTimeout(async () => {
    const filtered = latching_tool.down(paths);
    if (filtered.length !== 0) {
      console.log(
        new Date().toLocaleTimeString("hu"),
        "Rebundle: ",
        filtered,
      );
    }
    for (const file of filtered) {
      const file_to_write = "dist/" +
        file.split("/").at(-1)?.replace(".ts", ".js");

      const result = await bundle(new URL(`file://${file}`)).catch((e: any) => {
        console.error(e);
      });
      if (!result) return;
      const new_f_content = encoder.encode(result.code);
      Deno.writeFile(file_to_write, new_f_content);
    }
  }, 500);
}
