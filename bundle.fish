#!/bin/fish

set bin_folder "src/bin"
set out_dir "dist"

# Ensure folder
mkdir -p "$out_dir"

# Bundle each binary -- TODO :: Only if changed ??
for ts_file in (/bin/ls "$bin_folder");
  set js_file (string replace ".ts" ".js" $ts_file)
  deno bundle "$bin_folder/$ts_file" > "$out_dir/$js_file"
end
