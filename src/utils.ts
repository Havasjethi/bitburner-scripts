export function text_to_power(text_: string): number {
  const xy = text_.toUpperCase();

  switch (xy) {
    case "":
      return 1;
    case "K":
      return 1_000;
    case "KK":
      return 1_000_000;
    case "M":
      return 1_000_000;
    case "B":
      return 1_000_000_000;
    case "T":
      return 1_000_000_000_000;
    case "Q":
      return 1_000_000_000_000_000;
    default:
      throw new Error("Unavlid number format: " + xy);
  }
}

export function is_array<T>(obj: T | T[]): obj is T[] {
  return (obj as any).constructor.name === "Array";
}

type ScriptArg = string | number | boolean;

export function parse_number(num_str: ScriptArg | string[]): number {
  if (is_array(num_str)) {
    throw new Error("Expected string but got: " + num_str);
  }
  if (typeof num_str !== "string") {
    throw new Error("Expected string but got: " + num_str);
  }
  const reduced = num_str.replace("_", "");
  const index = reduced.search(/[a-zA-Z]/);

  const num = index > 0
    ? +reduced.slice(0, index) *
      text_to_power(reduced.slice(index))
    : +reduced;

  return num;
}
