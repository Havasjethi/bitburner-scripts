// deno-lint-ignore-file no-inferrable-types

import { NS, Server } from "./api.ts";
import { MAX_COST } from "./consts.ts";

export function get_logger(ns: NS) {
  return (...args: any[]) => {
    if (args.length === 1) {
      ns.tprint(JSON.stringify(args[0], undefined, 4));
    } else {
      ns.tprint(JSON.stringify(args, undefined, 4));
    }
  };
}

export type HostName = string;

export const HACK = "hack.js";
export const WEAK = "weak.js";
export const GROW = "grow.js";
export const SHARE = "share.js";

export const SHARE_COST = 4;

export const FILES_TO_COPY = [
  HACK,
  WEAK,
  GROW,
  SHARE,
];

export function get_local_hosts(ns: NS, current_host: string): HostName[] {
  return ns.scan(current_host);
}

export function global_scan(
  ns: NS,
  current_host: string,
  depth: number = Infinity,
): Server[] {
  const hosts = [] as Server[];
  get_logger(ns)("Before", depth, current_host);
  scan_visit(ns, current_host, (host) => {
    ns.tprint("Hello");
    hosts.push(ns.getServer(host));
  }, depth);

  return hosts;
}

export function scan_visit(
  ns: NS,
  starting: HostName,
  action: (x: HostName) => void,
  area: number = Infinity,
): void {
  const scanned_items = new Set<string>();
  let total_rounds = area;
  const hosts_to_scan = [starting];

  do {
    for (const h of hosts_to_scan.splice(0)) {
      scanned_items.add(h);

      const ss = ns.scan(h);
      ns.tprint("X: " + JSON.stringify(ss));
      for (const scanned_host of ss) {
        if (scanned_items.has(scanned_host)) {
          continue;
        }

        scanned_items.add(scanned_host);
        hosts_to_scan.push(scanned_host);
        action(scanned_host);
      }
    }
  } while (hosts_to_scan.length !== 0 && total_rounds-- > 1);
}

export function get_hosts_to_hack(ns: NS, current_host: string): HostName[] {
  const available_hosts = ns.scan(current_host);
  const hosts_to_hack: HostName[] = [];

  for (const host of available_hosts) {
    const server = ns.getServer(host);

    if (!server.hasAdminRights && server.maxRam !== 0) {
      hosts_to_hack.push(server.hostname);
    }
  }

  return hosts_to_hack;
}

export function get_target_host(ns: NS): HostName {
  // const available_hosts = ns.scan(current_host);
  const available_hosts = ns.scan("home");
  const hosts_to_hack: HostName[] = [];

  for (const host of available_hosts) {
    const server = ns.getServer(host);

    if (!server.hasAdminRights && server.maxRam !== 0) {
      hosts_to_hack.push(server.hostname);
    }

    const available_ram = server.maxRam - server.ramUsed;
    const money_cap = (server.moneyMax || 0) - (server.moneyAvailable || 0);
    const growth = server.serverGrowth;
  }

  throw new Error("TODO :: Finish");
  return "";
}

export function run_cracks(ns: NS, hostname: string) {
  silent_try(ns.sqlinject.bind(null, hostname));
  silent_try(ns.httpworm.bind(null, hostname));
  silent_try(ns.relaysmtp.bind(null, hostname));
  silent_try(ns.ftpcrack.bind(null, hostname));
  silent_try(ns.brutessh.bind(null, hostname));
  silent_try(ns.nuke.bind(null, hostname));
}

export function silent_try(callable: () => void) {
  try {
    callable();
  } catch {
    // Empty !!
  }
}

export function copy_hack_files(ns: NS, target_host: string) {
  ns.scp(FILES_TO_COPY, target_host, "home");
}

export async function choose(
  ns: NS,
  display: string,
  options: string[],
): Promise<string> {
  return await (ns.prompt(
    display,
    {
      type: "select",
      choices: options,
    },
  ) as Promise<string>);
}

export function move_hack_files(ns: NS, host_server: string) {
  if (!ns.scp(FILES_TO_COPY, host_server, "home")) {
    throw new Error("Unable to move files to server.");
  }
}

export type DeployAndShareOptions = {
  relative?: number;
  max?: number;
};

export function deploy_and_share(
  ns: NS,
  server: Server,
  options: DeployAndShareOptions = {},
): number {
  const host_server = server.hostname;
  const available_ram = server.maxRam - server.ramUsed;

  const relative = options?.relative || 1;
  const max = options?.max || Infinity;
  const total_to_run = Math.min(
    Math.floor(available_ram / SHARE_COST * relative),
    max,
  );

  if (total_to_run > 0) {
    move_hack_files(ns, host_server);
    const res = ns.exec(SHARE, host_server, { threads: total_to_run });
    get_logger(ns)(
      `Deployed share scripts: ${total_to_run} # ${host_server} # ${res}`,
    );
  }
  return total_to_run;
}

export type HackRatios = [number, number, number];

/*
 * @returns The deployed script count
 */
export function deploy_to_remote(
  ns: NS,
  server: Server,
  [weak, grow, hack]: HackRatios,
  hack_target: string,
  resource_allocation = 1,
): number {
  if (resource_allocation < 0 || resource_allocation > 1) {
    throw new Error(
      "Invalid property - resource_allocation too low / big: " +
        resource_allocation,
    );
  }
  const host_server = server.hostname;
  const available_ram = server.maxRam - server.ramUsed;
  const total = available_ram / MAX_COST;

  const PARTS = weak + grow + hack;
  const part = Math.floor(total / PARTS * resource_allocation);
  const total_adjusted = part * PARTS;

  if (part - 0.1 <= 0 || total_adjusted === 0) {
    return 0;
  }

  get_logger(ns)(
    `${server.hostname} | Free: ${
      ns.formatRam(available_ram)
    } | Able to run: ${total_adjusted} scripts...`,
  );

  move_hack_files(ns, host_server);

  if (host_server === "home") {
    ns.run(WEAK, { threads: part * weak }, hack_target, 5000);
    ns.run(GROW, { threads: part * grow }, hack_target, 5000);
    ns.run(HACK, { threads: part * hack }, hack_target, 5000);
  } else {
    ns.exec(WEAK, host_server, { threads: part * weak }, hack_target, 5000);
    ns.exec(GROW, host_server, { threads: part * grow }, hack_target, 5000);
    ns.exec(HACK, host_server, { threads: part * hack }, hack_target, 5000);
  }

  get_logger(ns)(`# Scripts deployed # ${server.hostname}`);
  return total_adjusted;
}

export function parse_rations(ratios: string): HackRatios {
  const parsed = ratios
    .split("-")
    .map((e) => +e) as HackRatios;

  if (parsed.length !== 3) {
    throw new Error(`Wrong ratios. must be 3 , got: ${ratios.length}`);
  }

  return parsed;
}

export function calculate_power(
  power: number,
  fix_gb: number,
) {
  return power >= 0 ? Math.pow(2, power) : fix_gb;
}
