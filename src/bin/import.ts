import { NS } from "../api.ts";
import { SERVER_PORT } from "../consts.ts";
import { get_logger } from "../lib.ts";

const files = [
  "hack_hosts.js",
  "import.js",
  "infiltration_lister.js",
  "global_scan.js",
  "root_accessor.js",
  "remote_deploy.js",
  "self_deploy.js",
  "share.js",
  "test.js",

  "buy_server.js",
  "upgrade_server.js",

  "kill_all.js",
  "share_all.js",
];

export async function main(ns: NS) {
  const log = get_logger(ns);

  log("Staring file import ... ");

  const promises = [];
  for (const f of files) {
    const target_url = `http://127.0.0.1:${SERVER_PORT}/${f}`;
    log(target_url);
    promises.push(ns.wget(target_url, f));
  }
  await Promise.all(promises);

  log("Files imported");
}
