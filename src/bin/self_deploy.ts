import { NS } from "../api.ts";
import { deploy_to_remote, get_logger, parse_rations } from "../lib.ts";

export async function main(ns: NS) {
  const log = get_logger(ns);
  log("Starting deploying...");

  const args = ns.flags([
    ["target", "n00dles"],
    ["ratios", "11-8-1"],
  ]);

  deploy_to_remote(
    ns,
    ns.getServer("home"),
    parse_rations(args.ratios as string),
    args.target as string,
  );
}
