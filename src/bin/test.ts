import { NS } from "../api.ts";
import {
  copy_hack_files,
  deploy_and_share,
  deploy_to_remote,
  get_logger,
  global_scan,
  parse_rations,
  run_cracks,
  scan_visit,
} from "../lib.ts";

export async function main(ns: NS) {
  const hackLevel = ns.getPlayer().skills.hacking;
  const flags = ns.flags([
    ["depth", 1],
    ["target", "n00dles"],
    ["ratios", "20-20-1"],
    ["sleep_time", 0], // Sleep time in sec
  ]);

  const hack_target = flags.target as string;
  const ratios = parse_rations(flags.ratios as string);
  const depth = +flags.depth;
  const sleep_time = (+flags.sleep_time) * 1000;
  const log = get_logger(ns);

  for (const server of global_scan(ns, "home", depth)) {
    log(server.hostname);
    if (server.maxRam === 0) {
      continue;
    }

    if (!server.hasAdminRights) {
      if (hackLevel >= (server.hackDifficulty || Infinity)) {
        run_cracks(ns, server.hostname);
        copy_hack_files(ns, server.hostname);
      } else {
        log("SA??");
        continue;
      }
    }

    const total_deployed = deploy_to_remote(ns, server, ratios, hack_target);

    if (total_deployed === 0) {
      deploy_and_share(ns, server);
    } else if (sleep_time > 0) {
      log(`Sleeping for  a bit: (${sleep_time} ms)`);
      await ns.sleep(sleep_time);
    }
  }
}
