import { NS } from "../api.ts";
import { calculate_power, get_logger } from "../lib.ts";

export async function main(ns: NS) {
  const log = get_logger(ns);
  const args = ns.flags([
    ["power", -1],
    ["gb", -1],
  ]);

  const power = +args.power;
  const gb = +args.gb;

  const fix_size = calculate_power(power, gb);

  if (fix_size <= 0) {
    log("--power", "to 2 ^ <power> GB");
    log("--gb", "to direct GB");
    return;
  }

  const r = ns.purchaseServer("epyc", fix_size);
  log(r);
}
