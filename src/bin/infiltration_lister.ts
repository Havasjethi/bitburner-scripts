import { NS } from "../api.ts";
import { get_logger } from "../lib.ts";

export async function main(ns: NS) {
  const log = get_logger(ns);
  for (const loca of ns.infiltration.getPossibleLocations()) {
    log(
      ns.infiltration.getInfiltration(loca.name),
    );
  }
}
