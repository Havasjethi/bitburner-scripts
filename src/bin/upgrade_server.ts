import { NS } from "../api.ts";
import { calculate_power, get_logger } from "../lib.ts";
import { parse_number } from "../utils.ts";

export async function main(ns: NS) {
  const log = get_logger(ns);
  const args = ns.flags([
    ["power", -1],
    ["gb", -1],
    ["budget", ""],
  ]);

  const power = +args.power;
  const gb = +args.gb;
  const budget = args.budget === "" ? Infinity : parse_number(args.budget);

  const fix_size = calculate_power(power, gb);

  if (fix_size <= 0) {
    log("--power", "to 2 ^ <power> GB");
    log("--gb", "to direct GB");
    log("--budget", "maximum budget");
    return;
  }

  const servers = ns.getPurchasedServers();
  let spent_budget = 0;

  for (const server of servers) {
    const server_ram = ns.getServerMaxRam(server);

    if (server_ram < fix_size) {
      const upgrade_cost = ns.getPurchasedServerUpgradeCost(server, fix_size);

      if ((spent_budget + upgrade_cost) < budget) {
        spent_budget += upgrade_cost;
        const result = ns.upgradePurchasedServer(server, fix_size);
        log(
          `Upgrading server: ${server} ${
            result
              ? "SUCCESS"
              : "FAIL required: " + ns.formatNumber(upgrade_cost)
          }`,
        );
      } else {
        log("No budget for: " + server);
      }
    }
  }
}
