import { NS } from "../api.ts";
import { deploy_and_share, DeployAndShareOptions, scan_visit } from "../lib.ts";

export async function main(ns: NS) {
  const args = ns.flags([
    ["depth", 1],
    ["relative", -1],
  ]);

  const depth = +args.depth;
  const relative = +args.relative;
  const options: DeployAndShareOptions = {};

  if (relative >= 0 && relative <= 1) {
    options.relative = relative;
  }

  scan_visit(ns, "home", (host) => {
    deploy_and_share(ns, ns.getServer(host), options);
  }, depth);
}
