import { NS } from "../api.ts";
import {
  copy_hack_files,
  get_hosts_to_hack,
  get_logger,
  run_cracks,
  scan_visit,
} from "../lib.ts";

// TODO :: Script for hacking with backdoor
export async function main(ns: NS) {
  const current_host = ns.getHostname();
  const args = ns.flags([
    ["depth", 1],
    ["start_host", "home"],
  ]);

  const start_host = args.start_host as string;
  const depth = +args.depth;

  const log = get_logger(ns);
  const hosts_to_hack: string[] = [];

  scan_visit(ns, start_host, (e) => {
    const server = ns.getServer(e);
    if (!server.hasAdminRights && server.hostname !== "darkweb") {
      hosts_to_hack.push(server.hostname);
    }
  }, depth);
  // const hosts_to_hack = get_hosts_to_hack(ns, current_host);

  log(hosts_to_hack);

  for (const host of hosts_to_hack) {
    run_cracks(ns, host);
    copy_hack_files(ns, host);
  }
}
