import { NS } from "../api.ts";
import { choose, deploy_to_remote, get_logger, parse_rations } from "../lib.ts";

export async function main(ns: NS) {
  const log = get_logger(ns);
  log("Starting deploying...");

  const flags = ns.flags([
    ["server", ""],
    ["target", ""],
    ["ratios", "11-8-1"],
  ]);

  const optional_targets = [ // TODO :: Add backdoor-ed targets
    "n00dles",
    "joesguns",
  ];

  const servers = ns.getPurchasedServers();

  const host_server = flags.server
    ? flags.server as string
    : await choose(ns, "Select HOST server: ", servers);

  const hack_target = flags.target
    ? flags.target as string
    : await choose(ns, "Select hack target: ", optional_targets);

  const server = ns.getServer(host_server);

  deploy_to_remote(
    ns,
    server,
    parse_rations(flags.ratios as string),
    hack_target,
  );
}
