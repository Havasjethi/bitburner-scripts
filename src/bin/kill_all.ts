import { NS } from "../api.ts";
import { calculate_power, get_logger } from "../lib.ts";
import { parse_number } from "../utils.ts";

export async function main(ns: NS) {
  const servers = ns.getPurchasedServers();

  for (const server of servers) {
    ns.killall(server);
  }
}
