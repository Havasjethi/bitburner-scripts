import { NS } from "../api.ts";
import { get_logger, global_scan, GROW, HACK, WEAK } from "../lib.ts";

export async function main(ns: NS) {
  const depth = +(ns.args[0] || 1);
  if (depth !== depth) {
    throw new Error("Baj");
  }
  const result = global_scan(ns, "home", depth); //

  ns.write("result.txt", JSON.stringify(result));
  ns.tprint("Total: ", result.length);
  ns.tprint("ns.args[0]?: ", ns.args[0]);
}
